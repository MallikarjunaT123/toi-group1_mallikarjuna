package com.perfectotoi.listeners;

import org.openqa.selenium.Capabilities;
import org.testng.Reporter;

import com.qmetry.qaf.automation.step.StepExecutionTracker;
import com.qmetry.qaf.automation.ui.webdriver.CommandTracker;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebDriverCommandListener;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElementCommandListener;

public class ios
		implements
			com.qmetry.qaf.automation.step.QAFTestStepListener,
			QAFWebDriverCommandListener,
			QAFWebElementCommandListener {

	@Override
	public void onFailure(StepExecutionTracker stepExecutionTracker) {
		Reporter.log("Test step is failed");
	}

	@Override
	public void beforExecute(StepExecutionTracker stepExecutionTracker) {
		Reporter.log("Test step will execute");
	}

	@Override
	public void afterExecute(StepExecutionTracker stepExecutionTracker) {
		Reporter.log("Test step is executed");

	}

	@Override
	public void beforeCommand(QAFExtendedWebDriver driver,
			CommandTracker commandHandler) {
		Reporter.log("Webdriver command will execute");

	}

	@Override
	public void afterCommand(QAFExtendedWebDriver driver, CommandTracker commandHandler) {
		Reporter.log("Webdriver command is executed");

	}

	@Override
	public void onFailure(QAFExtendedWebDriver driver, CommandTracker commandHandler) {
		Reporter.log("Webdriver command execution failed");

	}

	@Override
	public void beforeInitialize(Capabilities desiredCapabilities) {
		Reporter.log("Webdriver will initialaize");

	}

	@Override
	public void onInitialize(QAFExtendedWebDriver driver) {
		Reporter.log("Webdriver initialized");
	}

	@Override
	public void onInitializationFailure(Capabilities desiredCapabilities, Throwable t) {
		Reporter.log("Webdriver initialization failed");
	}

	@Override
	public void beforeCommand(QAFExtendedWebElement element,
			CommandTracker commandTracker) {
		Reporter.log("Try to find webelement");
	}

	@Override
	public void afterCommand(QAFExtendedWebElement element,
			CommandTracker commandTracker) {
		Reporter.log("Webelement got successfully");

	}

	@Override
	public void onFailure(QAFExtendedWebElement element, CommandTracker commandTracker) {
		Reporter.log("Webelement is not getting");

	}

}
