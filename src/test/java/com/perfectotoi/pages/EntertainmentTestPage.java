package com.perfectotoi.pages;

import java.util.List;

import com.perfectotoi.CommonStep.EntertainmentPageCommon;
import com.perfectotoi.component.EntertainmentPageItem;
//import com.flipkartapptest.components.ProductlistcomponentTestPage;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;

public class EntertainmentTestPage extends WebDriverBaseTestPage<WebDriverTestPage> implements EntertainmentPageCommon {
	
	
	@FindBy(locator = "list.bollywood")
	private List<EntertainmentPageItem> productlist;
	
	@FindBy(locator = "fisrtMovie.bollywood")
	private QAFWebElement fisrtMovieBollywood;
	
	@FindBy(locator = "movieName.bollywood")
	private QAFWebElement movieNameBollywood;
	
	@FindBy(locator = "cast.bollywood")
	private  QAFWebElement castBollywood;
	
	@FindBy(locator = "director.bollywood")
	private QAFWebElement directorBollywood;
	
	@FindBy(locator="reviewpage.header")
	private QAFWebElement reviewpageHeader;
	

	public QAFWebElement getReviewpageHeader() {
		return reviewpageHeader;
	}
	
	public QAFWebElement getFisrtMovieBollywood() {
		return fisrtMovieBollywood;
	}

	public  QAFWebElement getMovieNameBollywood() {
		return movieNameBollywood;
	}

	public QAFWebElement getCastBollywood() {
		return castBollywood;
	}

	public QAFWebElement getDirectorBollywood() {
		return directorBollywood;
	}

	public List<EntertainmentPageItem> getProductlist() {
		return productlist;
	}

	protected void openEntertainmentPage(PageLocator locator, Object... args) {
	}

	
	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}
	

}
