package com.perfectotoi.pages;

import java.util.List;

import com.perfectotoi.CommonStep.HomePageCommon;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class HomeTestPage extends WebDriverBaseTestPage<WebDriverTestPage> implements HomePageCommon {

	@FindBy(locator = "skip.homepage")
	private QAFWebElement skipHomepage;
	@FindBy(locator = "sensex.section.homepage")
	private QAFWebElement sensexSectionHomepage;
	@FindBy(locator = "naivgationbar.homepage")
	private QAFWebElement naivgationbarHomepage;
	@FindBy(locator = "list.tab.homepage")
	private List<QAFWebElement> listTabHomepage;
	@FindBy(locator = "contentdesc.all")
	private QAFWebElement contentdescAll;
	@FindBy(locator = "text.all")
	private QAFWebElement textAll;
	@FindBy(locator = "btn.moreoptions.homepage")
	private QAFWebElement btnMoreoptionsHomepage;
	@FindBy(locator = "text.contains.common")
	private QAFWebElement textContainsCommon;
	@FindBy(locator = "btn.loginorregister.homepage")
	private QAFWebElement btnLoginorregisterHomepage;
	@FindBy(locator = "txt.email.homepage")
	private QAFWebElement txtEmailHomepage;
	@FindBy(locator = "txt.password.homepage")
	private QAFWebElement txtPasswordHomepage;
	@FindBy(locator = "btn.login.homepage")
	private QAFWebElement btnLoginHomepage;
	@FindBy(locator = "text.city.homepage")
	private QAFWebElement textCityHomepage;
	@FindBy(locator = "text.logout.homepage")
	private QAFWebElement textLogoutHomepage;
	@FindBy(locator = "text.Yes.logoutconfirmation")
	private QAFWebElement textYesLogoutconfirmation;
	@FindBy(locator = "story.text")
	private QAFWebElement storyText;
	@FindBy(locator = "menu.icon")
	private QAFWebElement menuIcon;
	@FindBy(locator = "star.icon")
	private QAFWebElement starIcon;
	@FindBy(locator = "back.button")
	private QAFWebElement backButton;
	@FindBy(locator = "saved_stories_icon")
	private QAFWebElement saved_stories_icon;
	@FindBy(locator = "saved_story.text")
	private QAFWebElement saved_storyText;
	@FindBy(locator = "citysearchfield.homepage")
	private QAFWebElement citysearchfieldHomepage;
	@FindBy(locator = "radiobutton.searchdata")
	private QAFWebElement radiobuttonSearchdata;
	@FindBy(locator = "btn.back.navigation")
	private QAFWebElement btnBackNavigation;
	@FindBy(locator = "label.sports.sportPage")
	private QAFWebElement labelSportsSportPage;
	@FindBy(locator = "tab.navigation.hompages")
	private QAFWebElement tabNavigationHompages;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
	}

	public QAFWebElement getSkipHomepage() {
		return skipHomepage;
	}

	public QAFWebElement getSensexSectionHomepage() {
		return sensexSectionHomepage;
	}

	public QAFWebElement getNaivgationbarHomepage() {
		return naivgationbarHomepage;
	}

	public List<QAFWebElement> getListTabHomepage() {
		return listTabHomepage;
	}

	public QAFWebElement getContentdescAll() {
		return contentdescAll;
	}

	public QAFWebElement getTextAll() {
		return textAll;
	}

	public QAFWebElement getBtnMoreoptionsHomepage() {
		return btnMoreoptionsHomepage;
	}

	public QAFWebElement getTextContainsCommon() {
		return textContainsCommon;
	}

	public QAFWebElement getBtnLoginorregisterHomepage() {
		return btnLoginorregisterHomepage;
	}

	public QAFWebElement getTxtEmailHomepage() {
		return txtEmailHomepage;
	}

	public QAFWebElement getTxtPasswordHomepage() {
		return txtPasswordHomepage;
	}

	public QAFWebElement getBtnLoginHomepage() {
		return btnLoginHomepage;
	}

	public QAFWebElement getTextCityHomepage() {
		return textCityHomepage;
	}

	public QAFWebElement getTextLogoutHomepage() {
		return textLogoutHomepage;
	}

	public QAFWebElement getTextYesLogoutconfirmation() {
		return textYesLogoutconfirmation;
	}

	public QAFWebElement getStoryText() {
		return storyText;
	}

	public QAFWebElement getMenuIcon() {
		return menuIcon;
	}

	public QAFWebElement getStarIcon() {
		return starIcon;
	}

	public QAFWebElement getBackButton() {
		return backButton;
	}

	public QAFWebElement getSaved_stories_icon() {
		return saved_stories_icon;
	}

	public QAFWebElement getSaved_storyText() {
		return saved_storyText;
	}

	public QAFWebElement getCitysearchfieldHomepage() {
		return citysearchfieldHomepage;
	}

	public QAFWebElement getRadiobuttonSearchdata() {
		return radiobuttonSearchdata;
	}

	public QAFWebElement getBtnBackNavigation() {
		return btnBackNavigation;
	}

	public QAFWebElement getLabelSportsSportPage() {
		return labelSportsSportPage;
	}

	public QAFWebElement getTabNavigationHompages() {
		return tabNavigationHompages;
	}

}
