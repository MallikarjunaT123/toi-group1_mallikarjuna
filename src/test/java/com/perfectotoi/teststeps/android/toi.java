package com.perfectotoi.teststeps.android;

import java.util.HashMap;
import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.JavascriptExecutor;

import com.perfectotoi.component.EntertainmentPageItem;
import com.perfectotoi.pages.EntertainmentTestPage;
import com.perfectotoi.pages.HomeTestPage;
import com.perfectotoi.util.utility;
import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.step.CommonStep;
import com.qmetry.qaf.automation.step.NotYetImplementedException;
import com.qmetry.qaf.automation.step.QAFTestStep;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebElement;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;

public class toi extends WebDriverBaseTestPage<WebDriverTestPage>
		implements
			com.perfectotoi.CommonStep.CommonStep {
	HomeTestPage homePage = new HomeTestPage();
	EntertainmentTestPage entertain = new EntertainmentTestPage();

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
		driver.get("/");
	}

	@QAFTestStep(description = "user scroll down till business")
	public void userScrollDownTillBusiness() throws InterruptedException {
		Reporter.log("scroll up to Business section");

		while (!homePage.getSensexSectionHomepage().isPresent()) {
			Reporter.log("in while loop");
			JavascriptExecutor js = (JavascriptExecutor) driver;
			HashMap<String, String> scrollObject = new HashMap<String, String>();
			scrollObject.put("direction", "down");
			js.executeScript("mobile:scroll", scrollObject);
			Thread.sleep(8000);
			Reporter.log("scroll down completed");
		}
		Reporter.log(
				"scrolling successful" + homePage.getSensexSectionHomepage().getText());
	}

	@QAFTestStep(description = "user opens navigation bar")
	public void userOpensNavigationBar() {
		homePage.getNaivgationbarHomepage().click();
		Reporter.log("Navigation bar is opened");
	}

	@QAFTestStep(description = "user get all tab list")
	public void userGetAllTabList() {

		List<QAFWebElement> list = homePage.getListTabHomepage();
		for (int i = 0; i < list.size(); i++) {
			Reporter.log("tab name is " + list.get(i).getText());

		}
	}

	public QAFExtendedWebElement creatElement(String loc, String key) {

		return new QAFExtendedWebElement(
				String.format(ConfigurationManager.getBundle().getString(loc), key));
	}
	
	//Click on Button
	@QAFTestStep(description = "user clicks on {0}")
	public void userClicksOn(String btn) {
		// CommonStep.waitForPresent("contentdesc.all");
		//Click on Star Icon
		if (btn.equalsIgnoreCase("star")) {
			homePage.getStarIcon().click();
			Reporter.logWithScreenShot("Successfully Clicked on Start Icon",
					MessageTypes.Pass);
			homePage.getBackButton().click();
		} else {
			CommonStep.click(String.format(
					ConfigurationManager.getBundle().getString("contentdesc.all"), btn));

		}
	}

	@QAFTestStep(description = "user clicks on {0} button")
	public void Small(String btn) {
		CommonStep.click(String
				.format(ConfigurationManager.getBundle().getString("text.all"), btn));
	}

	@QAFTestStep(description = "user search for {0} and text is {1}")
	public void userSearchFor(String loc) {

		new QAFExtendedWebElement(loc).sendKeys("");

	}

	@QAFTestStep(description = "user clicks on {0} button and send character {1}")
	public void userClicksOnButtonAndSendCharacter(String btn, String text) {
		String loc = String
				.format(ConfigurationManager.getBundle().getString("text.all", btn));
		CommonStep.sendKeys(text, loc);

	}

	
	
    //Verification of Saved Story
	@QAFTestStep(description = "verify saved story")
	public void verifySavedStory() {
		homePage.getSaved_storyText().isPresent();
		Reporter.logWithScreenShot("Successfully Verified Saved Story",
				MessageTypes.Pass);
	}

	@QAFTestStep(description = "user login to the application")
	public void userLoginToTheApplication() {
		QAFExtendedWebElement element =
				new QAFExtendedWebElement("btn.loginorregister.homepage");
		element.waitForVisible();
		element.click();
		Reporter.logWithScreenShot("click on register/loginbutton button successfully",
				MessageTypes.Pass);
		CommonStep.clear("txt.email.homepage");
		CommonStep.sendKeys("datsdhana@gmail.com", "txt.email.homepage");
		CommonStep.sendKeys("D@tsdhana1", "txt.password.homepage");
		QAFExtendedWebElement btnLogin = new QAFExtendedWebElement("btn.login.homepage");
		btnLogin.click();
		Reporter.logWithScreenShot("login performed successfully", MessageTypes.Pass);

	}

	@QAFTestStep(description = "user verifies the profile page")
	public void userVerifiesTheProfilePage() {

		Validator.verifyThat("user successfully navigated to profile page",
				CommonStep.verifyPresent("text.city.homepage"), Matchers.is(true));

	}
	@QAFTestStep(description = "user {0} the application")
	public void userLogoutTheApplication(String data) {
		/*
		 * Map<String, Object> params = new HashMap<>();
		 * params.put("start", "20%,80%");
		 * params.put("end", "50%,80%");
		 * params.put("duration", "3");
		 * Object res = driver.executeScript("mobile:touch:swipe", params);
		 */

		/*
		 * Dimension dim=driver.manage().window().getSize();
		 * int height=dim.getHeight();
		 * int width=dim.getWidth();
		 * int x=width/2;
		 * int starty=(int)(height*0.80);
		 * int endy=(int)(height*0.20);
		 * ((Object) driver).swipe();
		 * String a="dhana";
		 * a.
		 */

		// TouchAction action = new TouchAction((MobileDriver) driver);
		/*
		 * action.moveTo(element).perform();
		 * element.click();
		 * QAFExtendedWebElement element1 =
		 * new QAFExtendedWebElement("text.Yes.logoutconfirmation");
		 * element1.click();
		 * Reporter.logWithScreenShot("logout successfully performed",
		 * MessageTypes.Pass);
		 * JavascriptExecutor js = (JavascriptExecutor) driver;
		 * HashMap<String, String> scrollObject = new HashMap<String, String>();
		 * scrollObject.put("direction", "down");
		 * scrollObject.put("element", ((RemoteWebElement) element).getId());
		 * js.executeScript("mobile: scroll", scrollObject);
		 */

	}

	@QAFTestStep(description = "user navigates to {0} page")
	public void userNavigatesToPage(String data) {
		homePage.getBtnMoreoptionsHomepage().waitForVisible();
		homePage.getBtnMoreoptionsHomepage().click();
		QAFExtendedWebElement txtSetting = new QAFExtendedWebElement(String
				.format(ConfigurationManager.getBundle().getString("text.all"), data));
		txtSetting.click();
	}

	@QAFTestStep(description = "user selects the {0} city")
	public String userSelectsTheCity(String data) {
		homePage.getTextCityHomepage().click();
		CommonStep.sendKeys(data, "citysearchfield.homepage");
		homePage.getRadiobuttonSearchdata().waitForVisible();
		homePage.getRadiobuttonSearchdata().click();
		homePage.getTextCityHomepage().waitForVisible();
		String cityName = homePage.getTextCityHomepage().getText();
		return cityName;

	}

	@QAFTestStep(description = "user navigates to home page")
	public void userNavigatesToHomePage() {
		homePage.getBtnBackNavigation().click();
		Reporter.logWithScreenShot("user navigated to homepage", MessageTypes.Pass);
	}

	@QAFTestStep(description = "user verifies the {0} tab")
	public void userVerifiesTheTab(String data) {
		String str = data.toUpperCase();
		QAFExtendedWebElement cityTab = new QAFExtendedWebElement(String
				.format(ConfigurationManager.getBundle().getString("text.all"), str));
		String cityText = cityTab.getText();
		Validator.verifyThat("the city name tab in homepage is verified ",
				cityText.contains(str), Matchers.is(true));

	}

	/**
	 * Clicking on India tab.
	 */
	@QAFTestStep(description = "user navigate to {0}")
	public void userNavigateToPage(String page) {
		List<QAFWebElement> list = homePage.getListTabHomepage();
		if (page.equalsIgnoreCase("India page")) {
			for (int i = 0; i < list.size(); i++) {
				if (list.get(i).getText().equalsIgnoreCase("India")) {
					list.get(i).click();
					break;
				}
			}
		}
			
		//clicking in Saved Stories
		else if(page.equalsIgnoreCase("Saved Stories")) {
			homePage.getMenuIcon().click();
			homePage.getSaved_stories_icon().click();
			Reporter.logWithScreenShot("Successfully Clicked on Saved Stories",
					MessageTypes.Pass);
		}

	}

	/**
	 * verify India page header
	 */
	@QAFTestStep(description = "user verify India page header")
	public void userVerifySportsPageHeader() {
		Validator.verifyThat("validating India page header",
				homePage.getLabelSportsSportPage().isPresent(), Matchers.equalTo(true));
	}
	
	//clicks on the top story
	@QAFTestStep(description = "User Open story") 
	public void userOpenStory() {
		homePage.getStoryText().click();
		Reporter.logWithScreenShot("Successfully Clicked on Story", MessageTypes.Pass);
	}

	/**
	 *closing app
	 * @throws InterruptedException 
	 */
	@QAFTestStep(description = "user close the app")
	public void userCloseTheApp() throws InterruptedException {
		utility.closeApp("TOI");
	}
	@QAFTestStep(description = "user clicks on Entertainment button")
	public void clickEntertainment() {
		CommonStep.click(String
				.format(ConfigurationManager.getBundle().getString("text.all"), "Entertainment"));
	}

	/*@QAFTestStep(description = "user print the list of movie reviews")
	public void userPrintTheListOfMovieReviews() {
		waitForPageToLoad();
		for (int i = 0; i < entertain.getProductlist().size(); i++) {
			EntertainmentPageItem item = entertain.getProductlist().get(i);
			Reporter.log(item.toString());
			Reporter.log("Movie name "+item.getMovieNameBollywood().getText());
			
			//Reporter.log("Movie name :   " + entertain.getProductlist().get(i).getText());
		}

	}*/
	@QAFTestStep(description="user select first movie from the list")
	public void selectFirstmovieInTheList()
	{
		/*CommonStep.click(String.format(
				ConfigurationManager.getBundle().getString("movieName.bollywood"), "Avengers: Infinity War"));*/
		
	}
	
	@QAFTestStep(description ="user verify the first movie details in the list")
	public void verifyFirstMovieDetailsInList()
	{
		
		
		/*String MovieName=entertain.getMovieNameBollywood().getText();
		String cast=entertain.getCastBollywood().getText();
		String director=entertain.getDirectorBollywood().getText();
		
		Reporter.log("Movie Name : "+MovieName+ " Cast : " +cast+ "Director : "+director);*/
	}

}
