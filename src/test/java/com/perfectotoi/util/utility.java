package com.perfectotoi.util;

import java.util.HashMap;
import java.util.Map;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;

public class utility {

	
	public static void openApp(String appName) throws InterruptedException {
		Map<String, Object> params = new HashMap<>();
		params.put("name", appName);
	
	new WebDriverTestBase().getDriver().executeScript("mobile:application:open", params);
	}
	
	public static void closeApp(String appName) throws InterruptedException {
		Map<String, Object> params = new HashMap<>();
		params.put("name", appName);
		
	new WebDriverTestBase().getDriver().executeScript("mobile:application:close", params);
	}
	
	
}
